<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        return $this->findCashPaymentRecursive($amount, [1, 2, 5, 10, 20, 50, 100], []); // call recursive function
    }

    private function findCashPaymentRecursive($amount, $coins, $payment) {
        // base case
        if ($amount == 0 && empty($coins)) {
            return $payment; // return final payment
        }

        $coin = $coins[count($coins) - 1]; // highest coin type
        $payment[$coin] = 0; // initialize number of coins to 0

        // find next highest coin type
        while ($coin > $amount) {
            array_pop($coins);

            // if there are no more coin types, call base case
            if ($amount == 0 && empty($coins)) {
                return $payment;
            }

            $coin = $coins[count($coins) - 1]; // set new highest coin type
            $payment[$coin] = 0;
        }

        $num_coins = floor($amount / $coin); // number of coins for current coin type
        $payment[$coin] = $num_coins; // set number of coins
        $remainder = $amount % $coin; // remainder of amount
        array_pop($coins); // remove highest coin type

        return $this->findCashPaymentRecursive($remainder, $coins, $payment); // call function recursively
    }
}