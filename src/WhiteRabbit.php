<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $content = file_get_contents($filePath); // read the file
        $content_filtered = strtolower(preg_replace('/[^a-z]/i', '', $content)); // remove unwanted characters
        $char_count = count_chars($content_filtered, 1); // count number of occurrences for each unique character

        // convert keys in $char_count from byte values to char values
        foreach ($char_count as $char => $count) {
            $char_count[chr($char)] = $count;
            unset($char_count[$char]); // remove old keys
        }

        return $char_count;
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $char_count = $parsedFile;
        asort($char_count); // sort the array by values from lowest to highest
        $char_keys = array_keys($char_count); // make an array with the keys from $char_count
        $median_index = floor((count($char_count) - 1) / 2); // find the median index
        $median_char = $char_keys[$median_index]; // the character which occurrences are the median
        $occurrences = $char_count[$median_char]; // the number of occurrences of that character

        /* The test parameters for the files text3.txt and text5.txt are wrong,
         * and is not the correct median characters. These tests are supposed to fail. */

        return $median_char;
    }
}